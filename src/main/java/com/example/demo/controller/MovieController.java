package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import com.example.demo.domain.Movie;
import com.example.demo.domain.MovieInput;
import com.example.demo.domain.MovieSearch;
import com.example.demo.domain.Person;
import com.example.demo.domain.PersonInput;
import com.example.demo.repository.MovieRepository;
import com.example.demo.repository.PersonRepository;

@Controller
public class MovieController {
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private MovieRepository movieRepository;

    @QueryMapping
    public List<Person> people(@Argument Person person) {
        if (person == null) {
            return personRepository.findAll();
        }

        String firstName = person.getFirstName() == null ? "" : person.getFirstName();
        String lastName = person.getLastName() == null ? "" : person.getLastName();

        return personRepository.findByQuery(firstName, lastName);
    }

    @QueryMapping
    public Person personById(@Argument Integer id) {
        return personRepository.findById(id).get();
    }

    @QueryMapping 
    public List<Movie> movies(@Argument MovieSearch search) {
        if (search == null) {
            return movieRepository.findAll();
        }

        String name = search.getName() == null ? "" : search.getName();
        Integer yearFrom = search.getYearFrom() == null ? 0 : search.getYearFrom();
        Integer yearTo = search.getYearTo() == null ? Integer.MAX_VALUE : search.getYearTo();
        List<String> genres = search.getGenres() == null ? null : listToLowerCase(search.getGenres());
        Integer ageLimitFrom = search.getAgeLimitFrom() == null ? 0 : search.getAgeLimitFrom();
        Integer ageLimitTo = search.getAgeLimitTo() == null ?  Integer.MAX_VALUE  : search.getAgeLimitTo();
        Integer ratingFrom = search.getRatingFrom() == null ? 0 : search.getRatingTo();
        Integer ratingTo = search.getRatingTo() == null ?  Integer.MAX_VALUE  : search.getRatingTo();
        List<Person> actors = search.getActors() == null ? null : search.getActors();
        Person director = search.getDirector() == null ? null : search.getDirector();
        String synopsis = search.getSynopsis() == null ? "" : search.getSynopsis();

        return movieRepository.findByQuery(
            name,
            yearFrom,
            yearTo,
            ageLimitFrom,
            ageLimitTo,
            ratingFrom,
            ratingTo,
            synopsis
        ).stream()
        .filter(movie -> genreMatch(movie, genres))
        .filter(movie -> personMatch(movie.getDirector(), director))
        .filter(movie -> actorsMatch(movie.getActors(), actors))
        .toList();
    }

    @QueryMapping
    public Movie movieById(@Argument Integer id) {
        return movieRepository.findById(id).get();
    }

    @MutationMapping
    public Movie updateOrCreateMovie(@Argument MovieInput input) {
        Movie movie = input.getMovie();
        return movieRepository.save(movie);
    }

    @MutationMapping
    public Person updateOrCreatePerson(@Argument PersonInput input) {
        Person person = input.getPerson();
        return personRepository.save(person);
    }

    protected boolean genreMatch(Movie movie, List<String> genres) {
        return genres == null ||
            listToLowerCase(movie.getGenres()).containsAll(genres);
    }

    protected boolean personMatch(Person person, Person searchPerson) {
        if (searchPerson == null) {
            return true;
        }

        if (person == null) {
            return false;
        }

        return (searchPerson.getFirstName() == null || person.getFirstName().toLowerCase().contains(searchPerson.getFirstName().toLowerCase())) &&
            (searchPerson.getLastName() == null || person.getLastName().toLowerCase().contains(searchPerson.getLastName().toLowerCase()));
    }

    protected boolean actorsMatch(List<Person> actors, List<Person> searchPersons) {
        if (searchPersons == null) {
            return true;
        }

        if (actors == null) {
            return false;
        }

        return searchPersons.stream().allMatch(
            searchPerson -> actors.stream().anyMatch(person -> personMatch(person, searchPerson))
        );
    }

    protected List<String> listToLowerCase(List<String> list) {
        return list.stream().map(value -> value.toLowerCase()).toList();
    }
}
