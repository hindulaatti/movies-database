package com.example.demo.domain;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieSearch {
    String name;
    Integer yearFrom;
    Integer yearTo;
    List<String> genres;
    Integer ageLimitFrom;
    Integer ageLimitTo;
    Integer ratingFrom;
    Integer ratingTo;
    List<Person> actors;
    Person director;
    String synopsis;
}
