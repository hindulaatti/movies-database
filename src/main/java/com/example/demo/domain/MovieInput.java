package com.example.demo.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.component.SpringContext;
import com.example.demo.repository.MovieRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieInput {
    MovieRepository movieRepository;

    private MovieRepository getMovieRepository() {
        if (movieRepository == null) {
            movieRepository = SpringContext.getBean(MovieRepository.class);
        }
        
        return movieRepository;
    }

    Integer id;
    String name;
    Integer year;
    List<String> genres;
    Integer ageLimit;
    Integer rating;
    List<PersonInput> actors;
    PersonInput director;
    String synopsis;
    
    public Movie getMovie() {
        if (id == null) {
            return new Movie(
                null, 
                name,
                year,
                genres,
                ageLimit,
                rating,
                getMovieActors(),
                getMovieDirector(),
                synopsis
                );
        }
        
        Movie movie = getMovieRepository().findById(id).get();

        movie.setName(name);
        movie.setYear(year);
        movie.setGenres(genres);
        movie.setAgeLimit(ageLimit);
        movie.setRating(rating);
        movie.setActors(getMovieActors());
        movie.setDirector(getMovieDirector());
        movie.setSynopsis(synopsis);

        return movie;
    }

    List<Person> getMovieActors() {
        if (actors == null || actors.size() == 0) {
            return new ArrayList<Person>();
        }

        return actors.stream()
            .map(input -> input.getPerson())
            .collect(Collectors.toList());
    }

    Person getMovieDirector() {
        if (director == null) {
            return null;
        }

        return director.getPerson();
    }
}
