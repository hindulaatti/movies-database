package com.example.demo.domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "movies")
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Integer id;

    @Column
    String name;

    @Column
    Integer year;

    @ElementCollection
    List<String> genres;

    @Column
    Integer ageLimit;

    @Column
    Integer rating;

    @ManyToMany(targetEntity=Person.class, fetch=FetchType.EAGER, cascade = {CascadeType.MERGE})
    List<Person> actors;

    @ManyToOne(targetEntity=Person.class, fetch=FetchType.EAGER, cascade = {CascadeType.MERGE})
    Person director;

    @Column
    String synopsis;
}
