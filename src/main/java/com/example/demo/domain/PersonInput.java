package com.example.demo.domain;

import com.example.demo.component.SpringContext;
import com.example.demo.repository.PersonRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonInput {
    PersonRepository personRepository;

    private PersonRepository getPersonRepository() {
        if (personRepository == null) {
            personRepository = SpringContext.getBean(PersonRepository.class);
        }
        
        return personRepository;
    }

    Integer id;
    String firstName;
    String lastName;

    public Person getPerson() {
        if(id == null) {
            return createNewPerson();
        }

        // .get() throws an exception, 
        // should handle for 404 but needs more thinking 
        // e.q. when saving whole movie where one person with ID is missing from database
        Person person = getPersonRepository().findById(id).get();

        person.setFirstName(firstName);
        person.setLastName(lastName);

        return person;
    }

    protected Person createNewPerson() {
        return new Person(
            null, 
            firstName,
            lastName
        );
    }
}
