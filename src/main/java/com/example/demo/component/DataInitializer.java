package com.example.demo.component;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.example.demo.domain.Movie;
import com.example.demo.domain.MovieInput;
import com.example.demo.domain.Person;
import com.example.demo.repository.MovieRepository;
import com.example.demo.repository.PersonRepository;
import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class DataInitializer {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private PersonRepository personRepository;

    @Value("${spring.jpa.hibernate.ddl-auto}")
    private String ddlAuto;
    
    @Value("classpath:movies-compact.json")
    Resource resourceFile;

    @PostConstruct
    private void initializeData() throws StreamReadException, DatabindException, IOException {
        if (!"create-drop".equals(ddlAuto)) {
            return;
        }

        readInitialData().forEach(movie -> movieRepository.save(convertMapToMovie(movie)));
    }

    private List<Map> readInitialData() throws StreamReadException, DatabindException, IOException {
        return new ObjectMapper().readValue(resourceFile.getFile(), List.class);
    }

    private Movie convertMapToMovie(Map data) {
        MovieInput input = new MovieInput();
        
        input.setName((String) data.get("name"));
        input.setYear((Integer) data.get("year"));
        input.setGenres((List<String>) data.get("genres"));
        input.setAgeLimit((Integer) data.get("ageLimit"));
        input.setRating((Integer) data.get("rating"));
        input.setSynopsis((String) data.get("synopsis"));
        
        Movie movie = input.getMovie();

        List<Object> actors = (List<Object>) data.get("actors");
        movie.setActors(
            actors.stream()
                .map(actor -> convertMapToPerson((Map) actor)).collect(Collectors.toList())
            );
        
        movie.setDirector(convertMapToPerson((Map) data.get("director")));

        return movie;
    }

    private Person convertMapToPerson(Map data) {
        String firstName = (String) data.get("firstName");
        String lastName = (String) data.get("lastName");

        Person person = personRepository.findByFirstNameAndLastName(firstName, lastName);

        if (person == null) {
            return personRepository.save(new Person(
                null, 
                firstName,
                lastName
            ));
        }

        return person;
    }
}
