package com.example.demo.repository;
 
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.domain.Movie;

public interface MovieRepository extends JpaRepository<Movie, Integer> {
    @Query("""
        SELECT m FROM Movie m
        WHERE lower(m.name) LIKE lower(concat('%', ?1, '%')) 
            AND m.year >= ?2 AND m.year <= ?3
            AND m.ageLimit >= ?4 AND m.ageLimit <= ?5
            AND m.rating >= ?6 AND m.rating <= ?7
            AND lower(m.synopsis) LIKE lower(concat('%', ?8, '%')) 
        """)
    List<Movie> findByQuery(
        String name,
        Integer yearFrom,
        Integer yearTo,
        Integer ageLimitFrom,
        Integer ageLimitTo,
        Integer ratingFrom,
        Integer ratingTo,
        String synopsis
    );
}
