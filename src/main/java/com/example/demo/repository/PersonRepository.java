package com.example.demo.repository;
 
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.demo.domain.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {
    @Query("SELECT p FROM Person p WHERE p.firstName = ?1 AND p.lastName = ?2")
    Person findByFirstNameAndLastName(String firstName, String lastName);

    @Query("""
        SELECT p FROM Person p 
        WHERE p.firstName LIKE lower(concat('%', ?1, '%')) 
            AND p.lastName LIKE lower(concat('%', ?2, '%'))
        """)
    List<Person> findByQuery(
        String firstName,
        String lastName);
}
